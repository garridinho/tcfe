% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS                            # Pic input begins with .PS
cct_init                       # Read in macro definitions and set defaults

l = 0.75                    # VariN_ables are allowed; default units are inches
Origin: Here                   # Position names are capitalized  
   source(up_ l,V);llabel(,V_{Th},);
   resistor(right_ l); llabel(,R_{Th},);
   {
     "b" wid 0.2*l ht 0.2*l  at Here.x+0.2*l,Here.y
   }
   {
     gap(down_ l)
   }
   resistor(down_ l); llabel(,R_{5},);
   {
     "b'" wid 0.2*l ht 0.2*l  at Here.x+0.2*l,Here.y
   }
   line to Origin
.PE
