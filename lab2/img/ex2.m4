% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS                            # Pic input begins with .PS
cct_init                       # Read in macro definitions and set defaults

l = 0.75                    # Variables are allowed; default units are inches
Origin: Here                   # Position names are capitalized
   source(up_ 2*l,V); llabel(,V_{cc},)
   line right l
   dot
   {
     resistor(down_ l); rlabel(,R_1,)
     dot
     {
     line right l*0.2
     {
       move up 0.1*l right 0.05*l
       {
         move up 0.1*l right 0.25*l
         "$v_b$" wid 0.2*l ht 0.2*l at Here.x,Here.y 
       }
       arrow right 0.5*l
     }
     gap(right_ l*0.6)
     line right l*0.2
     }
     resistor(down_ l); rlabel(,R_2,)
     dot
   }
   line right l
   resistor(down_ l);rlabel(,R_3,)
   dot 
   resistor(down_ l);rlabel(,R_4,)
   line left l
   dot
   ground(,)
   line to Origin
.PE
