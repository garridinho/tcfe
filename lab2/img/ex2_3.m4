% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS                            # Pic input begins with .PS
cct_init                       # Read in macro definitions and set defaults

l = 0.75                    # Variables are allowed; default units are inches
Origin: Here                   # Position names are capitalized
   line up l*0.8
   line right 0.2*l
   dot
   {
   line up 0.2*l
   resistor(right_ l); llabel(,R_1,);
   line down 0.2*l
   }
   line down 0.2*l
   resistor(right_ l); llabel(,R_2,);
   line up 0.2*l
   dot
   line right l
   {
   "b" wid 0.2*l ht 0.2*l  at Here.x+0.2*l,Here.y
   }
   gap(down_ l*0.8);
   {
   "b'" wid 0.2*l ht 0.2*l  at Here.x+0.2*l,Here.y
   }
   line left l
   dot
   {
   line up 0.2*l
   resistor(left_ l); llabel(,R_3,);
   line down 0.2*l
   }
   line down 0.2*l
   resistor(left_ l); llabel(,R_4,);
   line up 0.2*l
   dot

   line to Origin
   move right 2.4*l up 0.4*l
   arrow right l*0.5
   move down 0.25*l right 0.2*l

   line up 0.5*l
   resistor(right_ l); llabel(,R_{Th},);
   {
     "b" wid 0.2*l ht 0.2*l  at Here.x+0.2*l,Here.y
   }
   gap(down_ l*0.5);
   {
     "b'" wid 0.2*l ht 0.2*l  at Here.x+0.2*l,Here.y
   }
   line left l
.PE
