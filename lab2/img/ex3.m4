% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS                            # Pic input begins with .PS
cct_init                       # Read in macro definitions and set defaults

l = 0.75                    # Variables are allowed; default units are inches
Origin: Here                   # Position names are capitalized
   source(up_ l,V); llabel(,"$V_{cc}$",);
   resistor(right_ l); llabel(,R_1,);
   dot
   {
   resistor(down_ l); llabel(,R_2,);
   }
   dot
   resistor(right_ l); llabel(,R_3,);
   {
     {
       move down 0.5*l right 0.25*l
       "$v_b$" wid 0.2 ht 0.2 at Here.x,Here.y
     }
     move right 0.1*l down 0.1*l
     arrow down l*0.8
   }
   gap(down_ l);
   line left l
   dot
   ground(,)
   line to Origin
.PE
