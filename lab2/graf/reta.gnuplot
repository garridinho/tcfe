reset

# epslatex
set terminal epslatex size 9cm, 7cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'reta.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000f0f' lt 1 lw 2
unset key

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1
set grid back ls 12

set xrange [0:10]
set xtics 0,1,10

#axis labels
set xlabel '$V_{CC}/V$'
set ylabel '$V_{b}/V$'

set tics scale 2

r(x)=a*x
a=0.380556;

plot r(x) notitle ls 1
