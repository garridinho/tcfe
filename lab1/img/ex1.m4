% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS                            # Pic input begins with .PS
cct_init                       # Read in macro definitions and set defaults

l = 0.7                    # Variables are allowed; default units are inches
Origin: Here                   # Position names are capitalized
   source(up_ l,V);llabel(,V_{cc},);
   resistor(right_ l); rlabel(,"$R_1$",);
   dot
   {
     {
       "$v_{01}$" wid l*0.2 ht l*0.2 at (Here.x-0.2*l,Here.y-0.4*l)
     }
     move down 0.1*l
     arrow down_ l-0.2*l;
   }
   resistor(right_ l); rlabel(,"$R_2$",);
   dot 
   {
     {
       "$v_{02}$" wid l*0.2 ht l*0.2 at (Here.x-0.2*l,Here.y-0.4*l)
     }
   move down 0.1*l
   arrow down_ l-0.2*l;
   }
   resistor(right_ l); rlabel(,"$R_3$",);
   dot
   {
     {
       "$v_{03}$" wid l*0.2 ht l*0.2 at (Here.x-0.2*l,Here.y-0.4*l)
     }
   move down 0.1*l
   arrow down_ l-0.2*l;
   }
   resistor(right_ l); rlabel(,"$R_4$",);
   line down_ l
   line left 2*l
   dot 
   ground(,)
   line to Origin
.PE
